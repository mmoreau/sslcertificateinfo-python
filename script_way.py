#!/usr/bin/python3

import ssl
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

hostname = "google.com"

pem_data = ssl.get_server_certificate((hostname, 443))
cert = x509.load_pem_x509_certificate(pem_data.encode(), default_backend())

# Warning not always true
print(".:: PERIOD OF VALIDITY ::.")
print("\t- BEGINS  : ", cert.not_valid_before)
print("\t- EXPIRES : ", cert.not_valid_after)

# Warning not always true
print("\n.:: FINGERPRINTS ::.")
print("\t- SHA-256 : ", cert.fingerprint(hashes.SHA256()).hex().upper())
print("\t- SHA1    : ", cert.fingerprint(hashes.SHA1()).hex().upper())

print("\n.:: NAME ::. ")
[print("\t -", issuer.oid._name, ":", issuer.value.split("*.")[-1]) for issuer in cert.issuer]

print("\n.:: SUBJECT ::. ")
[print("\t -", subject.oid._name, ":", subject.value.split("*.")[-1]) for subject in cert.subject]

for ext in cert.extensions:

	if ext.value.__class__.__name__ == "SubjectAlternativeName":
		print("\n.:: DNS ::.")
		[print("\t -", (dns[0] + 1), ":", dns[-1].value.split("*.")[-1]) for dns in enumerate(ext.value)]

	if ext.value.__class__.__name__ == "CRLDistributionPoints":
		print("\n.:: CRL ::.")
		[print("\t -", crl.full_name[0].value) for crl in ext.value]

	if ext.value.__class__.__name__ == "AuthorityInformationAccess":
		[print("\n.:: " + oscp.access_method._name, "::.\n\t -", oscp.access_location.value) for oscp in ext.value]