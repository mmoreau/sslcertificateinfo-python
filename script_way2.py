#!/usr/bin/python3

import ssl 
import socket
from pprint import pprint

hostname = "google.com"

try:
    with socket.create_connection((hostname, 443)) as sock:
        
        context = ssl.create_default_context()
        
        # List of encryption algorithms supported by the SSL/TLS protocol : https://en.wikipedia.org/wiki/Cipher_suite#Supported_algorithms
        #pprint(context.get_ciphers())
        
    	with context.wrap_socket(sock, server_hostname=hostname) as ssock:
    
    		# Retrieve the certificate (PEM) : https://en.wikipedia.org/wiki/X.509#Certificate_filename_extensions
    		print(ssl.DER_cert_to_PEM_cert(ssock.getpeercert(True)))
    
    		# TLS Protocol Version : https://en.wikipedia.org/wiki/Transport_Layer_Security
    		print(ssock.version())
    
    		# Retrieves basic information from a website certificate (PEM) : https://en.wikipedia.org/wiki/Public_key_certificate
    		# Warning not always true (PERIOD OF VALIDITY & FINGERPRINTS)
    		pprint(ssock.getpeercert())
except:
    pass